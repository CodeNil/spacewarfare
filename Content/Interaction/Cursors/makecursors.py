# https://www.flaticon.com/packs/selection-and-cursors-40

from PIL import Image

def ResizeImage(path, newpath, size):
	img = Image.open(path)
	img = img.resize((size, size), Image.ANTIALIAS)
	img.save(newpath)

ResizeImage("cursor.png", "cursor_small.png", 40)
ResizeImage("cursor_hovered.png", "cursor_hovered_small.png", 40)
ResizeImage("cursor_moving.png", "cursor_moving_small.png", 40)
ResizeImage("cursor_move.png", "cursor_move_small.png", 40)
ResizeImage("rotate.png", "rotate_small.png", 40)
ResizeImage("target.png", "target_small.png", 40)
ResizeImage("torch.png", "torch_small.png", 40)
ResizeImage("line_vert.png", "line_vert_small.png", 60)
ResizeImage("line_hori.png", "line_hori_small.png", 60)